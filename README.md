# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Configuration
* App based on CodeIgniter 3
* Min requirement PHP 5.6
* URL configuration /application/config/development/config.php
* Database connection settings /application/config/development/database.php
* MySQL dump location /sql/dump.sql
* Postman collection with api requests https://www.getpostman.com/collections/cd690b4c91592c713389

### Admin Panel ###
* http://backend_test.local/backend
* login/password: admin@admin.com / admin

### Responses caching
* File cache used, possibly use memcached
* Response not cached for PUT, DELETE methods because response data must be NEW 

### API Endpoint ###
* http://backend_test.local/api
* All requests NEED 'Authorization' header with token '63HdWrpaqigPd1rcgRM2X5BS9vpJ4Aqg'
* All request NEED a raw body in JSON format, ex: {"name":"Test Name","cnp": "2451122301728"}
* All request&responses logged with MONOLOG to /applications/log/monolog-* and can be endpointed to ELASTICSEARCH cloud database
* Error response example
```
#!json
{
  "errorCode": 2000,
  "errorMessage": "Incorrect_Incoming_Variables",
  "data": {
    "serverTimeUtc": 1487328658252,
    "executionTime": "0.0392"
  }
}
```
* request PUT /api/customers_api/add {"name":"Test Name","cnp": "2451122301728"}
```
#!json
{
  "errorCode": 0,
  "errorMessage": null,
  "data": {
    "customerId": 51,
    "serverTimeUtc": 1487329369526,
    "executionTime": "0.0427"
  }
}
```
* request PUT /api/transactions_api/add {"customerId":"8","amount":13.88}
```
#!json
{
  "errorCode": 0,
  "errorMessage": null,
  "data": {
    "transactionId": 10,
    "customerId": 8,
    "amount": 13.88,
    "date": "2017/02/17 12:21:12",
    "serverTimeUtc": 1487330472088,
    "executionTime": "0.0463"
  }
}
```
* request POST /api/transactions_api/update {"transactionId":"10","amount":14.99}
```
#!json
{
  "errorCode": 0,
  "errorMessage": null,
  "data": {
    "transactionId": 10,
    "customerId": 8,
    "amount": 14.99,
    "date": "2017/02/17 12:31:23",
    "serverTimeUtc": 1487331083438,
    "executionTime": "0.0515"
  }
}
```
* request GET /api/transactions_api/get/8/10
```
#!json
{
  "errorCode": 0,
  "errorMessage": null,
  "data": {
    "transactionId": 10,
    "amount": 14.99,
    "date": "2017/02/17 12:39:12",
    "serverTimeUtc": 1487331552164,
    "executionTime": "0.0459"
  }
}
```
* request DELETE /api/transactions_api/delete/6
```
#!json
{
  "errorCode": 0,
  "errorMessage": null,
  "data": {
    "success": true,
    "serverTimeUtc": 1487331891057,
    "executionTime": "0.0431"
  }
}
```
* request POST /api/transactions_api/get
```
#!json
{
  "errorCode": 0,
  "errorMessage": null,
  "data": {
    "transactions": [
      {
        "id": "2",
        "customer_id": "15",
        "amount": "542.36",
        "created_at": "1487320042"
      },
      {
        "id": "3",
        "customer_id": "1",
        "amount": "756.00",
        "created_at": "1487320042"
      }
    ],
    "offset": 1,
    "limit": 2,
    "serverTimeUtc": 1487333539572,
    "executionTime": "0.0376"
  }
}
```

### Cron Job ###
* Cronjobs controller endpoint http://backend_test.local/cron
* 47 23 */2 * * php /your_path_to_app/index.php cron calc_sum 1 >> /your_path_to_app/application/logs/cron.log 2>&1
* for test run cron use http://backend_test.local/cron/calc_sum
* calculated sums by crobjob showed in dashboard, http://backend_test.local/backend