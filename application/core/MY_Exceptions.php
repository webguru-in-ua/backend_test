<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions
{

    public function __construct()
    {
        parent::__construct();
    }


    public function log_exception($severity, $message, $filepath, $line)
    {
        if (isset($this->levels[$severity])) {
            log_message($this->levels[$severity], $message . ' ' . $filepath . ' ' . $line);
        } else {
            log_message('error', 'Severity: ' . $severity . ' --> ' . $message . ' ' . $filepath . ' ' . $line);
        }

        //$severity = isset($this->levels[$severity]) ? $this->levels[$severity] : $severity;
        //log_message('error', 'Severity: '.$severity.' --> '.$message.' '.$filepath.' '.$line);
    }
}
