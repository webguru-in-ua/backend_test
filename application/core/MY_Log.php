<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/*
 * CodeIgniter Monolog integration
 *
 * Version 1.1.1
 * (c) Steve Thomas <steve@thomasmultimedia.com.au>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Monolog\Logger;
//use Monolog\ErrorHandler;
use Monolog\Handler\RotatingFileHandler;

//use Monolog\Handler\NewRelicHandler;
//use Monolog\Processor\IntrospectionProcessor;
use Monolog\ElasticLogstashHandler;

/**
 * replaces CI's Logger class, use Monolog instead
 *
 * see https://github.com/stevethomas/codeigniter-monolog & https://github.com/Seldaek/monolog
 *
 */
class MY_Log extends CI_Log
{
    // CI log levels
    protected $_levels = array(
        'ERROR' => 1,
        'WARNING' => 1,
        'PARSE' => 1,
        'NOTICE' => 1,
        'DEBUG' => 2,
        'INFO' => 3,
        'ALL' => 4
    );

    /*    public $levels = array(
            E_ERROR			=>	'Error',
            E_WARNING		=>	'Warning',
            E_PARSE			=>	'Parsing Error',
            E_NOTICE		=>	'Notice',
            E_CORE_ERROR		=>	'Core Error',
            E_CORE_WARNING		=>	'Core Warning',
            E_COMPILE_ERROR		=>	'Compile Error',
            E_COMPILE_WARNING	=>	'Compile Warning',
            E_USER_ERROR		=>	'User Error',
            E_USER_WARNING		=>	'User Warning',
            E_USER_NOTICE		=>	'User Notice',
            E_STRICT		=>	'Runtime Notice'
        );*/

    // config placeholder
    protected $config = array();

    protected $extra_data = array();
    protected $log_data = array();

    protected $log_handler = false;

    /**
     * prepare logging environment with configuration variables
     */
    public function __construct()
    {
        parent::__construct();

        // copied functionality from system/core/Common.php, as the whole CI infrastructure is not available yet
        if (!defined('ENVIRONMENT') OR !file_exists($file_path = APPPATH . 'config/' . ENVIRONMENT . '/monolog.php')) {
            $file_path = APPPATH . 'config/monolog.php';
        }
        // Fetch the config file
        if (file_exists($file_path)) {
            include($file_path);
        } else {
            exit('monolog.php config does not exist');
        }

        $this->config = $config;

        // make $config from config/monolog.php accessible to $this->write_log()

        $this->log = new Logger($this->config['monolog_channel']);
        // detect and register all PHP errors in this log hence forth
        //ErrorHandler::register($this->log);
        //if ($this->config['introspection_processor']) {
        // add controller and line number info to each log message
        //$this->log->pushProcessor(new IntrospectionProcessor());
        //}
        // decide which handler(s) to use
        foreach ($this->config['monolog_handlers'] as $value) {
            switch ($value) {
                case 'file':
                    $this->log_handler = new RotatingFileHandler($this->config['monolog_file_logfile']);
                    break;
                case 'elastic':
                    try {
                        $client = new Elasticsearch\Client(['hosts' => [$this->config['monolog_elasticsearch_client']]]);
                        $this->log_handler = new ElasticLogstashHandler($client, array('type' => $this->config['monolog_type']));
                    } catch (Exception $e) {

                        $this->log_handler = new RotatingFileHandler($this->config['monolog_file_logfile']);

                        //TODO: send notice
                        //$this->log = FALSE;
                        //return FALSE;
                        //var_dump($e);die();// 'Caught exception: ', $e->getMessage(), "\n";
                    }

                    break;
                default:
                    exit('monolog handler not supported');
            }
            //$formatter = new LineFormatter();
            $formatter = new Monolog\Formatter\LogstashFormatter($this->config['monolog_type'], null, '', '', 1);
            $this->log_handler->setFormatter($formatter);

            $this->log->pushHandler($this->log_handler);
        }
        $this->write_log('DEBUG', 'Monolog replacement logger initialized');
    }


    /**
     * Set additional extra data
     *
     * @param array $extra
     * @return bool
     */
    public function setExtraData($extra = array())
    {
        if (empty($extra))
            return FALSE;
        //$this->extra_data = array();
        $this->extra_data[] = $extra;
    }

    public function setData($key, $data = array())
    {
        if (empty($data))
            return FALSE;
        //$this->extra_data = array();
        $this->log_data[$key][] = $data;
    }


    /**
     * Write to defined logger. Is called from CodeIgniters native log_message()
     *
     * @param string $level
     * @param $msg
     * @return bool
     */
    public function write_log($level = 'error', $msg)
    {
        if ($this->log == FALSE) return FALSE;

        $level = strtoupper($level);

        if (!isset($this->_levels[$level]))
            $level = 'ERROR';

        if ((!isset($this->_levels[$level]) OR ($this->_levels[$level] > $this->_threshold))
            && !isset($this->_threshold_array[$this->_levels[$level]])
        ) {
            $this->log_data = [];
            $this->extra_data = [];
            return FALSE;
        }

        // Backtrace
        $ignore_functions = array(
            'log_message', 'write_log', 'log_exception',
            'call_user_func', 'call_user_func_array', '_remap'
        );

        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 5);
        $backtrace = array();
        foreach ($trace as $error) {

            if (!in_array($error['function'], $ignore_functions)) {
                $backtrace[] = array(
                    "file" => (isset($error['file'])) ? $error['file'] : null,
                    "line" => (isset($error['line'])) ? $error['line'] : null,
                    "function" => (isset($error['function'])) ? $error['function'] : null,
                    "class" => (isset($error['class'])) ? $error['class'] : null
                );
            }
        }

        // Prepare default log data
        $this->log_data = array_merge(array(
            'class' => $backtrace[0]['class'],
            'function' => $backtrace[0]['function'],
            'file' => $backtrace[0]['file'],
            'line' => $backtrace[0]['line'],
            'backtrace' => $backtrace
        ), $this->log_data);


        // Set and reset extra data
        if (!empty($this->extra_data)) {
            $this->log_data['extra_data'] = $this->extra_data;
        }

        if ($this->_levels[$level] <= $this->_threshold) {

            $log_file = './application/logs/local-' . date("Y-m-d") . '.log';

            switch ($level) {
                case 'ERROR':
                    try {
                        $this->log->addError($msg, $this->log_data);
                    } catch (Exception $e) {
                        file_put_contents($log_file, date("Y-m-d H:i:s") . " " . $e->getMessage(), FILE_APPEND | LOCK_EX);
                        file_put_contents($log_file, print_r((array)$this->log_data, true), FILE_APPEND | LOCK_EX);
                    }
                    break;
                case 'WARNING':
                    try {
                        $this->log->addWarning($msg, $this->log_data);
                    } catch (Exception $e) {
                        file_put_contents($log_file, date("Y-m-d H:i:s") . " " . $e->getMessage(), FILE_APPEND | LOCK_EX);
                        file_put_contents($log_file, print_r((array)$this->log_data, true), FILE_APPEND | LOCK_EX);
                    }
                    break;
                case 'NOTICE':
                    try {
                        $this->log->addNotice($msg, $this->log_data);
                    } catch (Exception $e) {
                        file_put_contents($log_file, date("Y-m-d H:i:s") . " " . $e->getMessage(), FILE_APPEND | LOCK_EX);
                        file_put_contents($log_file, print_r((array)$this->log_data, true), FILE_APPEND | LOCK_EX);
                    }
                    break;
                case 'DEBUG':
                    try {
                        $this->log->addDebug($msg, $this->log_data);
                    } catch (Exception $e) {
                        file_put_contents($log_file, date("Y-m-d H:i:s") . " " . $e->getMessage(), FILE_APPEND | LOCK_EX);
                        file_put_contents($log_file, print_r((array)$this->log_data, true), FILE_APPEND | LOCK_EX);
                    }
                    break;
                case 'ALL':
                case 'INFO':
                    try {
                        $this->log->addInfo($msg, $this->log_data);
                    } catch (Exception $e) {
                        file_put_contents($log_file, date("Y-m-d H:i:s") . " " . $e->getMessage(), FILE_APPEND | LOCK_EX);
                        file_put_contents($log_file, print_r((array)$this->log_data, true), FILE_APPEND | LOCK_EX);
                    }
                    break;
            }
        }


        $this->log_data = [];
        $this->extra_data = [];
        return true;
    }

    // Helper functions

    public function setError($msg, $extra = array())
    {
        $this->setExtraData($extra);
        $this->write_log('error', $msg);
    }

    public function setWarning($msg, $extra = array())
    {
        $this->setExtraData($extra);
        $this->write_log('warning', $msg);
    }

    public function setNotice($msg, $extra = array())
    {
        $this->setExtraData($extra);
        $this->write_log('notice', $msg);
    }

    public function setDebug($msg, $extra = array())
    {
        $this->setExtraData($extra);
        $this->write_log('debug', $msg);
    }

    public function setInfo($msg, $extra = array())
    {
        $this->setExtraData($extra);
        $this->write_log('info', $msg);
    }

    /*
     * Set API log
     */
    public function setAPIInfo($level, $message, $extra = array())
    {
        if ($this->log == FALSE) return FALSE;

        $data = array(
            'type' => 'API'
        );
        $data = array_merge($data, $extra);

        if (isset($level)) {
            $level = strtoupper($level);
            if ($this->_levels[$level] <= $this->config['monolog_client_log_threshold']) {
                switch ($level) {
                    case 'ERROR':
                        $this->log->addError($message, $data);
                        break;
                    case 'WARNING':
                        $this->log->addWarning($message, $data);
                        break;
                    case 'NOTICE':
                        $this->log->addNotice($message, $data);
                        break;
                    case 'DEBUG':
                        $this->log->addDebug($message, $data);
                        break;
                    case 'INFO':
                        $this->log->addInfo($message, $data);
                        break;
                    default:
                        $this->log->addError($message, $data);
                }
            }
        }


        return true;
    }

}