<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by kramarenkoa@digicode.net (admin.kramarenko@gmail.com)
 */

require_once APPPATH . 'third_party/REST/libraries/REST_Controller.php';

/**
 * Class ApiController
 *
 * @property CI_Loader $load
 * @property CI_Output $output
 * @property CI_DB_query_builder $db
 * @property CI_Config $config
 * @property CI_Session $session
 * @property CI_Cache $cache
 * @property CI_URI $uri
 * @property MY_Input $input
 * @property MY_Form_validation $form_validation
 * @property MY_Log $log
 *
 * @property Twig $twig
 * @property Ion_auth $ion_auth
 * @property Datatable $datatable
 *
 * @property Errors_model $errors_model
 * @property Customers_model $customers_model
 * @property Transactions_model $transactions_model
 */
class ApiController extends REST_Controller
{
    public $requestData = array();
    private $_responseData = array(
        'errorCode' => 0,
        'errorMessage' => null,
        'data' => array()
    );
    public $responseData = NULL;
    public $requestHeaders = array(
        'access_token' => NULL,
        'game_version' => NULL,
        'localization' => NULL
    );
    public $requestDebug = array();
    public $playerObject = NULL; // PlayerObject
    public $accountObject = NULL; // AccountObject

    private $methods_without_access_token = array('/api/customers_api/add');

    public $requestMethod = NULL;

    public $cache_key = '';
    public $cache_response = '';

    /*
     *
     */
    function __construct()
    {
        parent::__construct();

        $this->load->config('rest_api');
        $this->load->model('errors_model');

        // Request method
        $this->requestMethod = strtolower(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

        // request data, from POST'S BODY
        $this->requestData = json_decode($this->input->raw_input_stream, true);
        if (is_null($this->requestData))
            $this->requestData = array();

        if (!empty($this->input->raw_input_stream) && is_null($this->requestData))
            $this->responseError(2001); //Incorrect_Json_Data

        // headers
        $this->requestHeaders['access_token'] = $this->input->get_request_header('Authorization', true, true);

        // request debug information
        $this->requestDebug = array(
            'IP' => $this->input->ip_address(),
            'requestUrl' => $_SERVER['REQUEST_URI'],
            'requestMethod' => $_SERVER['REQUEST_METHOD'],
            'requestHeaders' => $this->input->request_headers()
        );
        if ($this->input->post()) $this->requestDebug['form-data'] = $this->input->post();


        // Log Request
        $this->log->setData('request_data', $this->requestData);
        $this->log->setDebug('API Request: ' . $_SERVER['REQUEST_URI'], $this->requestDebug);

        // Response headers
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key, Authorization, Localization');
        header('Access-Control-Allow-Methods: POST, OPTIONS');
        header('Access-Control-Max-Age: 300');
        header('Access-Control-Allow-Origin: *');

        // method headers validations
        $this->_accessTokenHeaderValidation();
    }


    /**
     *
     */
    private function _accessTokenHeaderValidation()
    {
        // load skipped methods from config
        if (!empty($this->config->item('rest_api_skip_access_token')))
            $this->methods_without_access_token = $this->config->item('rest_api_skip_access_token');

        foreach ($this->methods_without_access_token as $idx => $method)
            $this->methods_without_access_token[$idx] = strtolower($method);

        if (!in_array($this->requestMethod, $this->methods_without_access_token)) {

            // if no set `authorization` header return error
            if (empty($this->requestHeaders['access_token']))
                $this->responseError(8000, array('param' => 'Authorization')); // Invalid_AccessToken

            // TODO: need to change
            if ($this->requestHeaders['access_token'] !== '63HdWrpaqigPd1rcgRM2X5BS9vpJ4Aqg')
                $this->responseError(8000); // Invalid_AccessToken


            // caching
            if ($_SERVER['REQUEST_METHOD'] === 'GET')
                $this->cache_key = md5(serialize($_GET));

            if ($_SERVER['REQUEST_METHOD'] === 'POST' && $this->requestMethod == '/api/transactions_api/get')
                $this->cache_key = md5(serialize($_POST));

            $this->cache_response = $this->cache->file->get($this->cache_key);
            if ($this->cache_response) {
                $this->responseData = json_decode($this->cache_response, true);
                $this->responseData($this->responseData);

                return TRUE;
            }


            return TRUE;
        }


        return TRUE;
    }


    /**
     * Overwrite base response, add benchmark
     *
     * @param null $data
     * @param int $http_code
     * @param bool $continue
     */
    public function response($data = NULL, $http_code = 200, $continue = FALSE)
    {
        if (!empty($data))
            $this->_responseData = array_merge($this->_responseData, $data);

        $this->_responseData['data']['serverTimeUtc'] = round(microtime(true) * 1000);
        if ($this->config->item('rest_api_show_execution_time'))
            $this->_responseData['data']['executionTime'] = $this->benchmark->elapsed_time('total_execution_time_start');

        $this->log->setData('request_data', $this->requestData);
        $this->log->setData('response_data', $this->_responseData);
        $this->log->setDebug('API Response', $this->requestDebug);

        parent::response($this->_responseData, $http_code, $continue);
    }


    /**
     * Response Data
     *
     * @param null $data
     * @param int $http_code
     * @param bool|FALSE $continue
     */
    public function responseData($data = NULL, $http_code = 200, $continue = FALSE)
    {
        if (!empty($data))
            $this->_responseData['data'] = array_merge($this->_responseData['data'], $data);


        $this->_responseData['data']['serverTimeUtc'] = round(microtime(true) * 1000);
        if ($this->config->item('rest_api_show_execution_time'))
            $this->_responseData['data']['executionTime'] = $this->benchmark->elapsed_time('total_execution_time_start');

        if (!empty($this->cache_response)) {
            $this->_responseData['data']['cached'] = true;
        }

        if (!empty($data) && !empty($this->cache_key) && empty($this->cache_response)) {
            $this->cache->file->save($this->cache_key, json_encode($data), 60*60);
        }

        $this->log->setData('request_data', $this->requestData);
        $this->log->setData('response_data', $this->_responseData);
        $this->log->setDebug('API Response', $this->requestDebug);

        parent::response($this->_responseData, $http_code, $continue);
    }


    /**
     * Response error helper function
     * use to set defined error
     *
     * @param $error_code
     * @param null $data
     * @param int $http_code
     */
    public function responseError($error_code, $data = NULL, $http_code = 200)
    {
        if (!empty($data))
            $this->_responseData['data'] = array_merge($this->_responseData['data'], $data);

        $this->_responseData['errorCode'] = $error_code;

        //var_dump($error_message);
        $this->_responseData['errorMessage'] = $this->errors_model->getErrorMessageByCode($error_code);

        $this->_responseData['data']['serverTimeUtc'] = round(microtime(true) * 1000);
        $this->_responseData['data']['executionTime'] = $this->benchmark->elapsed_time('total_execution_time_start');

        $this->log->setData('request_data', $this->requestData);
        $this->log->setData('response_data', $this->_responseData);
        $this->log->setError('API ResponseError: ' . $error_code . ' ' . $this->_responseData['errorMessage'], $this->requestDebug);

        parent::response($this->_responseData, $http_code, FALSE);
    }


    public function _checkArrayKeys($array, $keys)
    {
        foreach ($keys as $key)
            if (!array_key_exists($key, $array)) {
                $this->responseError(2000); //Incorrect Incoming Variables
                return FALSE;
            }

        return TRUE;
    }
}


/**
 * Class MainController
 *
 * @property CI_Loader $load
 * @property CI_Output $output
 * @property CI_DB_query_builder $db
 * @property CI_Config $config
 * @property CI_Session $session
 * @property CI_Cache $cache
 * @property CI_URI $uri
 * @property MY_Input $input
 * @property MY_Form_validation $form_validation
 * @property MY_Log $log
 *
 * @property Twig $twig
 * @property Ion_auth $ion_auth
 * @property Datatable $datatable
 *
 * @property Errors_model $errors_model
 * @property Customers_model $customers_model
 * @property Transactions_model $transactions_model
 */
class AuthController extends CI_Controller
{
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data['application_name'] = config_item('application_name');

        $this->load->library('twig');
        $this->load->library('ion_auth');
    }
}


/**
 * Class AdminController
 */
class AdminController extends AuthController
{

    function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(true);

        // login redirect
        if (!$this->ion_auth->logged_in()) {
            redirect('backend/auth', 'refresh');
        } elseif (!$this->ion_auth->is_admin()) {
            $this->session->set_flashdata('validation_errors', 'You must be an admin to view this page');
            redirect('backend/auth', 'refresh');
        }

        // format current url
        $uri_segments = $this->uri->segment_array();
        array_shift($uri_segments);
        $this->data['_current_url'] = '/' . implode('/', $uri_segments);

        $this->data['_user'] = $this->ion_auth->user()->row();

        // user group for sidebar menu
        $this->data['_user_groups'] = $this->ion_auth->groups()->result();

        // additional info data
        $this->data['is_supported_memcached'] = ($this->cache->memcached->is_supported() && class_exists('Memcached')) ? 'Yes' : 'No';

        // Build main users many
        $users_menu = array('title' => 'Users', 'link' => '#users', 'nested' => array(
            array(
                'title' => $this->data['_user_groups'][0]->description,
                'link' => 'users/group/' . $this->data['_user_groups'][0]->id
            )
        ));
//        foreach ($this->data['_user_groups'] as $_user_group)
//            $users_menu['nested'][] = array('title' => $_user_group->description, 'link' => 'users/group/' . $_user_group->id);
        $users_menu['nested'][] = array('link' => 'users/create', 'title' => 'Create user', 'icon' => 'fa fa-circle-o text-green');

        // Build system menu
        $system_menu = array('title' => 'System Info', 'link' => '#system_info', 'nested' => array(
            array('title' => 'PHP Info', 'link' => 'phpinfo')
        )
        );


        $this->data['_side_bar'] = array(
            $users_menu
        , array('title' => 'Customers', 'link' => 'customers')
        , array('title' => 'Transactions', 'link' => 'transactions')
//        , array('title' => 'Events Log', 'link' => 'events_log')
        , $system_menu
        );

        $this->twig->set_layout('backend');
    }

}
