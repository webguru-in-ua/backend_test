<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Input extends CI_Input
{

    public function __construct()
    {
        parent::__construct();
    }

    // --------------------------------------------------------------------

    /**
     * Get Request Header
     * Add case insensitive
     *
     * Returns the value of a single member of the headers class member
     *
     * @param    string $index Header name
     * @param    bool $xss_clean Whether to apply XSS filtering
     * @return    string|null    The requested header on success or NULL on failure
     */
    public function get_request_header($index, $xss_clean = FALSE, $case_insensitive = false)
    {
        if($case_insensitive == false)
            return parent::get_request_header($index, $xss_clean);

        $index = strtolower($index);

        if (empty($this->headers)) {
            $this->request_headers();
        }

        $lc_headers = $this->headers;
        $lc_headers = array_change_key_case($lc_headers, CASE_LOWER);


        if (!isset($lc_headers[$index])) {
            return NULL;
        }

        return ($xss_clean === TRUE)
            ? $this->security->xss_clean($lc_headers[$index])
            : $lc_headers[$index];
    }

}
