<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by kramarenkoa@digicode.net (admin.kramarenko@gmail.com)
 */

/**
 * Extend base models class
 *
 * @property CI_Loader $load
 * @property CI_DB_query_builder $db
 * @property CI_Config $config
 * @property CI_Cache $cache
 * @property MY_Log $log
 */

class MY_Model extends CI_Model
{
    public $limit = 50;
    public $offset = 0;
    public $query_count = 0;

    private $tables = array(
        'users' => 'users'
    );

    public $t = null;

    public function __construct()
    {
        parent::__construct();

        // try load tables list from config
        $this->load->config('database_tables');
        if (!empty($this->config->item('database_tables_list')))
            $this->tables = $this->config->item('database_tables_list');

        $this->t = new stdClass();
        // get short access to tables names by $this->t->table_name ...
        foreach ($this->tables as $key => $value)
            $this->t->{$key} = $this->config->item('database_tables_prefix').$value;
    }


    /**
     * SELECT FOUND ROWS() AS cnt, use after SQL_CALC_FOUND_ROWS for get count after limit
     * @return int
     */
    public function get_rows_count_query()
    {
        $query = $this->db->query('SELECT FOUND_ROWS() AS cnt')->row();
        $this->query_count = $query->cnt;
        return $query->cnt;
    }

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */