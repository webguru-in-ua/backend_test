<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function time2seconds($str_time){
    sscanf($str_time, '%d:%d:%d', $hours, $minutes, $seconds);

    if(isset($seconds))
        return ($hours * 3600 + $minutes * 60 + $seconds);
    else
        return ($hours * 60 + $minutes);

}

function seconds2time($seconds, $format = 'H:i:s'){
    return gmdate($format, $seconds);
}