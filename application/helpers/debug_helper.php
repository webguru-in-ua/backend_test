<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function dd()
{
    echo '<pre>';

    foreach (func_get_args() as $arg)
        var_dump($arg);

    die();
}