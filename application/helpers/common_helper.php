<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function array_merge_by($key, $arrayA, $arrayB)
{
    //dd($arrayA, $arrayB);
    if (empty($arrayA))
        return $arrayB;

    if (empty($arrayB))
        return $arrayA;

    $output = array();

    $arrayAB = array_merge($arrayA, $arrayB);
    foreach ($arrayAB as $value) {
        $id = $value[$key];
        if (!isset($output[$id])) {
            $output[$id] = array();
        }
        $output[$id] = array_merge($output[$id], $value);
    }

    return array_values($output);
}


function csv2array($csv, $delimiter = ',')
{
    $lines = explode(PHP_EOL, $csv);
    $array = array();
    foreach ($lines as $line)
        $array[] = str_getcsv($line, $delimiter);
    return $array;
}

function array2csv($array, $delimiter = ',')
{
    $header = array_keys($array[0]);

    $csv = implode($delimiter, $header);
    foreach ($array as $line)
        $csv .= PHP_EOL.implode($delimiter, $line);

    return $csv;
}

/**
 * @param $str
 * @param bool $only_positive
 * @return bool|string
 */
function int_validation($str, $only_positive = false)
{
    $str = trim($str);
    if ((bool)preg_match('/^[\-+]?[0-9]+$/', $str)) {
        if ($only_positive == FALSE)
            return TRUE;
        else if ((int)$str >= 0)
            return TRUE;
    }
    return FALSE;
}

/**
 * @param $str
 * @param bool $only_positive
 * @return bool
 */
function int_numeric($str, $only_positive = false)
{
    $str = trim($str);
    if ((bool)preg_match('/^[\-+]?[0-9]*\.?[0-9]+$/', $str)) {
        if ($only_positive == FALSE)
            return TRUE;
        else if ((int)$str >= 0)
            return TRUE;
    }
    return FALSE;
}


/*function json_validation($str)
{
    dd($str);
    $str = trim($str);
    if (empty($str) || is_null($str)) return TRUE;
    if (json_decode($str)) return TRUE;
    return FALSE;
}*/