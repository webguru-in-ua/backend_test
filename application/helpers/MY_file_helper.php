<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Get Directory File Information
 *
 * Reads the specified directory and builds an array containing the filenames,
 * filesize, dates, and permissions
 *
 * Any sub-folders contained within the specified path are read as well.
 *
 * @param    string    path to source
 * @param    bool    Look only at the top level directory specified?
 * @param    bool    internal variable to determine recursion status - do not use in calls
 * @param    bool    only filename
 * @return    array
 */
function get_dir_file_info($source_dir, $top_level_only = TRUE, $_recursion = FALSE, $simply = FALSE, $original_source = '')
{
    static $_filedata = array();

    $original_source = rtrim(((empty($original_source)) ? $source_dir : $original_source), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

    $relative_path = $source_dir;

    if ($fp = @opendir($source_dir)) {
        // reset the array and make sure $source_dir has a trailing slash on the initial call
        if ($_recursion === FALSE) {
            $_filedata = array();
            $source_dir = rtrim(realpath($source_dir), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        }

        // Used to be foreach (scandir($source_dir, 1) as $file), but scandir() is simply not as fast
        while (FALSE !== ($file = readdir($fp))) {
            if (is_dir($source_dir . $file) && $file[0] !== '.' && $top_level_only === FALSE) {
                get_dir_file_info($source_dir . $file . DIRECTORY_SEPARATOR, $top_level_only, TRUE, $simply, $original_source . $file);
            } elseif ($file[0] !== '.') {

                if ($simply) {
                    $_filedata[] = $original_source . $file;
                } else {
                    $_filedata[$file] = get_file_info($source_dir . $file);
                    $_filedata[$file]['relative_path'] = $relative_path;
                    $_filedata[$file]['file_path'] = $original_source . $file;
                }

            }
        }

        closedir($fp);
        return $_filedata;
    }

    return FALSE;
}