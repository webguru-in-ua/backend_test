<?php
/**
 * Created by PhpStorm.
 * User: Alexandr Kramarenko
 * Date: 17.02.2017
 * Time: 10:20
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Get customer record by field=>value
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public function getOneBy($key, $value)
    {
        $query = $this->db->where($key, $value)
            ->limit(1)
            ->get($this->t->customers);

        if ($query && $query->num_rows() == 1)
            return $query->row();

        return NULL;
    }

    /**
     * Add new customer
     *
     * @param $data
     * @return integer
     */
    public function add($data)
    {
        $data['created_at'] = $data['updated_at'] = now();
        // insert new player
        $this->db->insert($this->t->customers, $data);
        return $this->db->insert_id();
    }


    //TODO::::


    /**
     * Get account record by field=>value pairs
     *
     * @param $key_value_arr
     * @return bool
     */
    public function getByArray($key_value_arr)
    {
        $query = $this->db->where($key_value_arr)
            ->limit(1)
            ->get($this->t->accounts);

        if ($query && $query->num_rows() == 1)
            //return $query->row();
            return $query->custom_row_object(0, 'AccountObject');

        return NULL;
    }


    public function getLastActiveAvatars($last_activity = 60 * 60 * 24)
    {
        $query = $this->db->select('facebook_uid')
            ->where('facebook_uid !=', '')
            ->where('last_login_at >', now() - $last_activity, true)
            ->order_by('last_login_at', 'DESC')
            ->get($this->accounts_model->t->accounts);

        if ($query && $query->num_rows() > 0) {

            $ids = [];
            foreach ($query->result_array() as $account)
                $ids[] = "http://graph.facebook.com/" . $account['facebook_uid'] . "/picture?type=square";

            return $ids;
        }


        return [];
    }


    /**
     * Update player access_token
     *
     * @param AccountObject $accountObject
     * @param array $data
     * @return bool
     */
    public function updateAccessToken(AccountObject $accountObject, $data = array())
    {
        $access_token = $this->_generateUniqueValue('access_token', 32);

        if ($access_token !== FALSE) {
            $data = array_merge(array(
                'access_token' => $access_token,
                'last_login_at' => now()
            ), $data);

            $this->_addAccessTokenToLog($accountObject, $data);

            return $this->update($accountObject, $data);
        }

        return FALSE;
    }

    private function _addAccessTokenToLog(AccountObject $accountObject, $data = array()) {
        $data = array(
            'account_id' => $accountObject->id,
            'access_token' => $data['access_token'],
            'device_udid' => (isset($data['device_udid']) && $data['device_udid'])?$data['device_udid']:$accountObject->device_udid,
            'created_at' => now(),
        );

        $this->db->insert($this->t->tokens_log, $data);
    }

    /**
     * Update player data
     *
     * @param AccountObject $accountObject
     * @param $account_data
     * @return bool
     */
    public function update(AccountObject $accountObject, $account_data)
    {
        $account_data['updated_at'] = now();

        $query = $this->db->where('id', $accountObject->id)
            ->set($account_data)
            ->update($this->t->accounts);

        if ($query == FALSE) {
            $this->log->setError('DB Error', $account_data);
            return FALSE;
        }

        // update PlayerObject fields
        foreach ($account_data as $key => $value) {
            $accountObject->$key = $value;
        }

        return TRUE;
    }


    /**
     * @param $account_id
     * @param $account_data
     * @return bool
     */
    public function updateData($account_id, $account_data)
    {
        $account_data['updated_at'] = now();

        return $this->db->where('id', $account_id)
            ->set($account_data)
            ->update($this->t->accounts);
    }


    /**
     * Set revalidate access token flag
     *
     * @param null $account_id
     * @return object
     */
    public function revalidateAccessToken($account_id = null)
    {
        if (!is_null($account_id))
            $this->db->where('id', $account_id);
        $this->db->set('is_access_token_revalidate', 1);
        return $this->db->update($this->t->accounts);
    }




    /**
     * Remove account
     *
     * @param $account_id
     * @return mixed
     */
    public function remove($account_id)
    {
        $this->db->trans_begin();
        $this->db->delete($this->t->payments_history, array('player_id' => $account_id));
        $this->db->delete($this->t->payments_log, array('player_id' => $account_id));
        $this->db->delete($this->t->balance_history, array('player_id' => $account_id));
        $this->db->delete($this->t->players, array('account_id' => $account_id));
        $this->db->delete($this->t->accounts, array('id' => $account_id));

        // END OF TRANSACTION
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->log->setError('Transaction rollback', array('Delete' => $account_id));
        } else {
            $this->db->trans_commit();
            return TRUE;
        }

        return FALSE;
    }


    /**
     * Safe delete
     *
     * @param $account_id
     * @param bool $uncheck
     * @return object
     */
    public function safe_delete($account_id, $uncheck = false)
    {
        return $this->db->set('is_deleted', ($uncheck) ? 0 : 1)
            ->where('id', $account_id)
            ->update($this->t->accounts);
    }


    /**
     * Helper function
     * Generate randomize token with length
     *
     * @param int $length
     * @return string
     */
    private function _createToken($length = 32)
    {
        if ($length % 2 != 0) {
            $length += 1;
            $hash = bin2hex(openssl_random_pseudo_bytes($length / 2));
            return substr($hash, 0, -1);
        }

        return bin2hex(openssl_random_pseudo_bytes($length / 2));
    }


    /**
     * Helper function
     * Try to generate unique value for db field
     *
     * @param $field
     * @param $size
     * @param int $attempt
     * @return bool|string
     */
    private function _generateUniqueValue($field, $size, &$attempt = 0)
    {
        if ($attempt >= 3) {
            $this->log->setError('Impossible create unique value for ' . $field);
            return FALSE;
        }

        $unique = $this->_createToken($size);
        if ($this->getBy($field, $unique) == true) {
            $this->log->setDebug('Try to create unique value for ' . $field);
            $attempt++;
            return $this->_generateUniqueValue($field, $size, $attempt);
        }

        return $unique;
    }

    /**
     * Get random facebook player
     * @param $account_id
     * @param int $limit
     * @return bool
     */
    public function getRandomAccount($account_id, $limit = 10){

        $query = $this->db->select('facebook_uid, username, gender')
            ->where('id !=', $account_id)
            ->where('facebook_uid !=', '')
            ->where('gender IS NOT NULL', null, false)
            ->where('gender !=', '')
            ->where('is_guest', 0)
            ->order_by('id', 'RANDOM')
            ->limit($limit)
            ->get($this->accounts_model->t->accounts);

        if ($query && $query->num_rows() > 0)
            return $query->result_array();

        return false;

    }


    /**
     * @param AccountObject $accountObject
     * @param bool|false $settings
     * @return array
     */
    public function formatAccountObject(AccountObject $accountObject, $settings = false)
    {
        $userData = array(
            'userId' => (int)$accountObject->id,
            'userEmail' => $accountObject->email,
            'accessToken' => $accountObject->access_token,
            'isGuest' => (bool)$accountObject->is_guest,
            'facebookId' => $accountObject->facebook_uid,
            'country' => $accountObject->location,
            'language' => $accountObject->locale,
            'gender' => $accountObject->gender,
            'firstName' => $accountObject->first_name,
            'lastName' => $accountObject->last_name,
            'userName' => $accountObject->username,
            'settings' => null
        );

        // retrieve additional data settings, if is needed
        if ($settings) {
            $userData['settings'] = array(
                'segmentation' => '',
                'ab_group' => 'master',
                'ab_group_id' => 0,
                'is_test_active' => false,
                'is_test_user' => (bool)$accountObject->is_test_user,
            );

            // retrieve test group if is set
            if ($accountObject->test_group_id > 0) {
                $this->load->model('test_groups_model');
                $test_group = $this->test_groups_model->getById($accountObject->test_group_id);
                if($test_group){
                    $userData['settings']['ab_group'] = $test_group['name'];
                    $userData['settings']['ab_group_id'] = (int)$test_group['id'];
                    $userData['settings']['is_test_active'] = (bool)$accountObject->is_test_group_active;

                    // retrieve segmentations
                    if (!empty($test_group['segmentations'])) {
                        $userData['settings']['segmentation'] = [];
                        foreach ($test_group['segmentations'] as $segmentation)
                            $userData['settings']['segmentation'][] = $segmentation['name'];
                        $userData['settings']['segmentation'] = implode('; ', $userData['settings']['segmentation']);
                    }
                }
            }
        }

        return $userData;
    }
}


/**
 * Class AccountObject
 */
class AccountObject
{
    public $id = 0;
    public $username = '';
    public $first_name = '';
    public $last_name = '';
    public $email = '';
    public $gender = '';
    public $location = '';
    public $locale = '';
    public $birthday = '';
    public $facebook_uid = '';
    public $access_token = '';
    public $device_udid = '';
    public $created_at = 0;
    public $last_login_at = 0;
    public $app_type = '';
    public $is_guest = 0;
    public $is_test_user = 0;
    public $is_deleted = 0;
    public $is_disabled = 0;
    public $test_group_id = 0;
    public $is_test_group_active = 0;
    public $is_access_token_revalidate = 0;

    function __construct($account_data = array())
    {
        $this->created_at = now();
        $this->last_login_at =$this->created_at;

        foreach ($account_data as $key => $value) {
            if (isset($this->{$key}))
                $this->{$key} = $value;
        }
    }

    public function __set($name, $value)
    {
        if (isset($this->$name))
            $this->$name = $value;
    }


    public function __get($name)
    {
        if (isset($this->$name))
            return $this->$name;

        throw new Exception('AccountObject property "'.$name.'" not found!');
    }
}