<?php
/**
 * Created by PhpStorm.
 * User: Alexandr Kramarenko
 * Date: 17.02.2017
 * Time: 10:14
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @return bool
     */
    public function getAll()
    {
        $query = $this->db->select('id')
            ->get($this->t->transactions);

        if ($query && $query->num_rows() > 0) {
            return $query->result();
        }

        return FALSE;
    }


    /**
     * Get transaction record by id
     *
     * @param $id
     * @return bool
     */
    public function getById($id)
    {
        $query = $this->db->where('id', $id)
            ->get($this->t->transactions);

        if ($query && $query->num_rows() == 1)
            return $query->row();

        return NULL;
    }


    public function getByWhere($where_array)
    {
        $offset = $limit = NULL;
        if(array_key_exists('offset', $where_array)){
            $offset = $where_array['offset'];
            unset($where_array['offset']);
        }

        if(array_key_exists('limit', $where_array)){
            $limit = $where_array['limit'];
            unset($where_array['limit']);
        }

        $query = $this->db->where($where_array)
            ->get($this->t->transactions, $limit, $offset);

        if ($query && $query->num_rows() > 0)
            return $query->result_array();

        return [];
    }


    /**
     * Add new transaction
     *
     * @param $data
     * @return mixed
     */
    public function add($data){
        $data['created_at'] = now();
        $this->db->insert($this->t->transactions, $data);
        return $this->db->insert_id();
    }


    /**
     * Delete
     *
     * @param $id
     * @return mixed
     */
    public function delete($id){
        return $this->db->where('id',$id)
            ->delete($this->t->transactions);
    }


    /**
     * Update
     *
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data){
        if(array_key_exists('id', $data))
            unset($data['id']);

        return $this->db->where('id', $id)
            ->update($this->t->transactions, $data);
    }


    /**
     * Get transaction record by customer_id
     *
     * @param $id
     * @return array
     */
    public function getByCustomerId($id)
    {
        $query = $this->db->where('customer_id', $id)
            ->get($this->t->transactions);

        if ($query && $query->num_rows() > 0)
            return $query->result_array();

        return [];
    }


    /**
     * Get all transactions sum
     *
     * @return array
     */
    public function getTransactionsSum()
    {
        $query = $this->db->get($this->t->transactions_sum);

        if ($query && $query->num_rows() > 0)
            return $query->result_array();

        return [];
    }


    /**
     * Calc transactions sum for previous day
     *
     * @return bool
     */
    public function calcTransactionsSum()
    {
        $sum = 0;
        $now = now(); // for time sync

        $previous_day = $now - 24 * 60 * 60;
        $query = "SELECT SUM(amount) AS 'sum' 
FROM `{$this->t->transactions}` 
WHERE created_at>$previous_day";

        $query = $this->db->query($query);
        if ($query && $query->num_rows() == 1)
            $sum = $query->row()->sum;

        $this->db->insert($this->t->transactions_sum, array(
            'sum' => $sum,
            'created_at' => $now
        ));

        return TRUE;
    }
}

