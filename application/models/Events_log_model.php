<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events_log_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     * @return bool
     */
    public function get($id)
    {
        $query = $this->db->where('id', $id)
            ->get($this->t->events_log);

        if ($query && $query->num_rows() == 1)
            return $query->row();

        return false;
    }

    /**
     * @return bool
     */
    public function getAll()
    {
        $query = $this->db->get($this->t->events_log);

        if ($query && $query->num_rows() > 0)
            return $query->result();

        return false;
    }


    /**
     * @param $data
     * @return object
     */
    public function add($data)
    {
        //foreach ($data as &$param)
        //$param = $this->db->escape_str($param);

        $data['created_at'] = now();
        $data['datetime_created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert($this->t->events_log, $data);
    }


    /**
     * Clear latest events
     *
     * @param int $ttl_days
     * @return mixed
     */
    public function clear($ttl_days = 7)
    {
        $ttl = $ttl_days * 24 * 60 * 60;
        return $this->db->where('created_at <', now() - $ttl)
            ->delete($this->t->events_log);
    }

}