<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by kramarenkoa@digicode.net (admin.kramarenko@gmail.com)
 */

class Errors_model extends MY_Model
{

    /*
     *
     */
    public function getErrorMessageByCode($code, $lang = 'en')
    {
        $query = $this->db->where('id', $code)
            ->where('lang', $lang)
            ->limit(1)
            ->get($this->t->error_codes);

        if ($query && $query->num_rows() == 1) {
            return $query->row()->message;
        }

        return 'Undefined error message';
    }

}