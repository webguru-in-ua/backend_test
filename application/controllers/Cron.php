<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller
{
    /**
     * crontab -l
     * crontab -e
     */

    public function index()
    {
        echo 'I\'m  cron';
    }

    /**
     * Transactions SUM
     * */
    // 47 23 */2 * * php /your_path_to_app/index.php cron calc_sum 1 >> /your_path_to_app/application/logs/cron.log 2>&1
    // for test run cron use http://backend_test.local/cron/calc_sum

    public function calc_sum()
    {
        $this->load->model('transactions_model');
        $this->transactions_model->calcTransactionsSum();

        echo "SUCCESSFUL";
        return TRUE;
    }

}
