<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		echo config_item('application_name');
	}

	public function not_found(){
		$this->output->set_status_header('404');
		echo 'page not found';
	}
}
