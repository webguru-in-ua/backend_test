<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by kramarenkoa@digicode.net (admin.kramarenko@gmail.com)
 */
class Transactions_api extends ApiController
{

    public function __construct()
    {

        parent::__construct();
    }


    /**
     * Transaction get
     *
     * params: customerId, transactionId
     * response: transactionId,amount,date
     */
    public function get_get($customer_id = 0, $transaction_id = 0)
    {
        if($customer_id < 1 || $transaction_id < 1)
            $this->responseError(2000); //Incorrect Incoming Variables

        $this->load->model('transactions_model');

        $exists_transaction = $this->transactions_model->getById($transaction_id);
        if ($exists_transaction == NULL)
            $this->responseError(3001); // 3001 Transaction_Not_Exists

        $this->responseData = array(
            "transactionId" => (int)$exists_transaction->id,
            "amount" => (float)$exists_transaction->amount,
            "date" => date('Y/m/d H:i:s')
        );

        $this->responseData($this->responseData);
    }



    /**
     * Transaction get filter
     *
     * params: customerId, amount, date, offset, limit
     * response: transactionId,amount,date
     */
    public function get_post()
    {
        $data = array();
        if(array_key_exists('customerId', $this->requestData))
            $data['customer_id'] = $this->requestData['customerId'];
        if(array_key_exists('amount', $this->requestData))
            $data['amount'] = $this->requestData['amount'];
        if(array_key_exists('created_at', $this->requestData))
            $data['created_at'] = $this->requestData['created_at'];
        if(array_key_exists('offset', $this->requestData))
            $data['offset'] = $this->requestData['offset'];
        if(array_key_exists('limit', $this->requestData))
            $data['limit'] = $this->requestData['limit'];

        $this->load->model('transactions_model');

        $exists_transactions = $this->transactions_model->getByWhere($data);
        if ($exists_transactions == NULL)
            $this->responseError(3001); // 3001 Transaction_Not_Exists

        $this->responseData = array(
            "transactions" => $exists_transactions,
            "offset" => (array_key_exists('offset', $data))?$data['offset']: null,
            "limit" => (array_key_exists('limit', $data))?$data['limit']: null,
        );

        $this->responseData($this->responseData);
    }


    /**
     * Transaction add
     *
     * params: customerId, amount
     * response: transactionId,customerId,amount,date
     */
    public function add_put()
    {
        // var_dump($this->requestData);
        $this->_checkArrayKeys($this->requestData, ['customerId', 'amount']);

        $data = array(
            'customer_id' => $this->requestData['customerId'],
            'amount' => $this->requestData['amount']
        );

        $this->load->model('customers_model');
        $exists_customer = $this->customers_model->getOneBy('id', $data['customer_id']);
        if ($exists_customer == NULL)
            $this->responseError(1002); // 1001 Customer not exists

        $this->load->model('transactions_model');
        $this->responseData = array(
            "transactionId" => (int)$this->transactions_model->add($data),
            "customerId" => (int)$data['customer_id'],
            "amount" => (float)$data['amount'],
            "date" => date('Y/m/d H:i:s')
        );

        $this->responseData($this->responseData);
    }


    /**
     * Transaction update
     *
     * params: transactionId, amount
     * response: transactionId,customerId,amount,date
     */
    public function update_post()
    {
        // var_dump($this->requestData);
        $this->_checkArrayKeys($this->requestData, ['transactionId', 'amount']);

        $data = array(
            'id' => $this->requestData['transactionId'],
            'amount' => $this->requestData['amount']
        );

        $this->load->model('transactions_model');
        $exists_transaction = $this->transactions_model->getById($data['id']);
        if ($exists_transaction == NULL)
            $this->responseError(3001); // 3001 Transaction_Not_Exists

        $this->transactions_model->update($data['id'], $data);

        $this->responseData = array(
            "transactionId" => (int)$exists_transaction->id,
            "customerId" => (int)$exists_transaction->customer_id,
            "amount" => (float)$data['amount'],
            "date" => date('Y/m/d H:i:s')
        );

        $this->responseData($this->responseData);
    }


    /**
     * Transaction delete
     *
     * params: transactionId, amount
     * response: transactionId,customerId,amount,date
     */
    public function delete_delete($transaction_id = 0)
    {
        if($transaction_id < 1)
            $this->responseError(2000); //Incorrect Incoming Variables

        $this->load->model('transactions_model');
        $exists_transaction = $this->transactions_model->getById($transaction_id);
        if ($exists_transaction == NULL)
            $this->responseError(3001); // 3001 Transaction_Not_Exists

        $this->transactions_model->delete($transaction_id);

        $this->responseData = array(
            "success" => true
        );

        $this->responseData($this->responseData);
    }

}