<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by kramarenkoa@digicode.net (admin.kramarenko@gmail.com)
 */
class Customers_api extends ApiController
{

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Customer add
     *
     * params: name, cnp
     * response: customerId
     */
    public function add_put()
    {
        $this->_checkArrayKeys($this->requestData, ['name','cnp']);

        $data = array(
            'name' => $this->requestData['name'],
            'cnp' => $this->requestData['cnp']
        );

        $this->load->model('customers_model');
        $exists_customer = $this->customers_model->getOneBy('cnp', $data['cnp']);
        if($exists_customer !== NULL)
            $this->responseError(1001); // 1001 Customer already exists

        $this->responseData["customerId"] = $this->customers_model->add($data);

        $this->responseData($this->responseData);
    }
}