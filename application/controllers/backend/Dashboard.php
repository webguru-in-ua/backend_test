<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AdminController
{

    function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(true);

        $this->data['page_header'] = 'Dashboard';
        $this->data['page_desc'] = 'Control panel';
    }


    /**
     * Index page - Dashboard
     */
    public function index()
    {
        $this->data['page_header'] = 'Dashboard';
        $this->data['page_desc'] = 'Control panel';

        $this->twig->display('dashboard.twig', $this->data);
    }


    /**
     * Get transactions sum list via post ajax
     * Server side datatable
     *
     * @throws Exception
     */
    public function get_list()
    {
        if (!$this->input->is_ajax_request()) die();

        $this->load->model('transactions_model');

        $this->load->library('Datatable', array(/*'model' => 'datatable/customers_datatable_model', 'rowIdCol' => 'id'*/));

        $this->datatable->appendToSelectStr = array();
        $this->datatable->fromTableStr = $this->transactions_model->t->transactions_sum;

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson(array(
            'created_at' => 'date'
        ));

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }


    /**
     * Simply get php info page
     */
    public function phpinfo()
    {
        phpinfo();
    }
}
