<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends AdminController
{
    function __construct()
    {
        parent::__construct();

        $this->data['page_header'] = 'Transactions';

        $this->load->model('transactions_model');
    }


    /**
     * Customers list
     *
     */
    public function index()
    {
        $this->data['page_desc'] = 'Transactions List';

        $this->twig->display('transactions/index.twig', $this->data);
    }


    /**
     * Get transaction list via post ajax
     * Server side datatable
     *
     * @param int $customer_id
     * @throws Exception
     */
    public function get_list($customer_id = 0)
    {
//        $this->output->enable_profiler(true);
        if (!$this->input->is_ajax_request()) die();

        $this->load->library('Datatable', array(/*'model' => 'datatable/customers_datatable_model', 'rowIdCol' => 'id'*/));

        $this->datatable->appendToSelectStr = array();
        $this->datatable->fromTableStr = $this->transactions_model->t->transactions;
        if ($customer_id > 0)
            $this->datatable->whereClauseArray = array(
                'customer_id' => $customer_id
            );

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson(array(
            'created_at' => 'date'
        ));

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }




}
