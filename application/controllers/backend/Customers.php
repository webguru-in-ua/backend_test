<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends AdminController
{
    function __construct()
    {
        parent::__construct();

        $this->data['page_header'] = 'Customers';

        $this->load->model('customers_model');
    }


    /**
     * Customers list
     *
     */
    public function index()
    {
        $this->data['page_desc'] = 'Customers List';

        $this->twig->display('customers/index.twig', $this->data);
    }



    /**
     * Get customers list via post ajax
     * Server side datatable
     *
     * @param bool $segment_id
     * @throws Exception
     */
    public function get_list()
    {
        if (!$this->input->is_ajax_request()) die();

        $this->load->library('Datatable', array(/*'model' => 'datatable/customers_datatable_model', 'rowIdCol' => 'id'*/));

        $this->datatable->appendToSelectStr = array();
        $this->datatable->fromTableStr = $this->customers_model->t->customers;
        $this->datatable->joinArray = array();


        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson(array(
            'created_at' => 'date',
            'updated_at' => 'date',
            'last_paid_at' => 'date',
        ));

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }


    /**
     * Remove all customer data
     * @param $customer_id
     */
    public function delete($customer_id)
    {
        $this->load->model('customers_model');
        $this->customers_model->remove($customer_id);

        redirect(backend_url("customers"), 'refresh');
    }

    /**
     * Safe delete
     * @param $customer_id
     */
    public function safe_delete($customer_id)
    {
        $this->load->model('customers_model');
        $this->customers_model->safe_delete($customer_id);

        redirect(backend_url("customers"), 'refresh');
    }

    public function undelete($customer_id)
    {
        $this->load->model('customers_model');
        $this->customers_model->safe_delete($customer_id, true);

        redirect(backend_url("customers"), 'refresh');
    }



    /**
     * Get customer login logs history table
     * @param bool $customer_id
     */
    public function login_log($customer_id = FALSE)
    {
        $this->load->model('customers_model');

        if ($customer_id == FALSE)
            redirect(backend_url('customers'), 'refresh');

        $this->data['page_desc'] = "customer History";

        $this->data['customer'] = $this->customers_model->getBy('account_id', $customer_id);

        $this->twig->display('customers/login_log.twig', $this->data);
    }


    /**
     * Ajax helper Get customers list
     * Server side datatable
     */
    public function get_transaction_list()
    {
        if (!$this->input->is_ajax_request()) die();

        $this->load->library('Datatable', array(/*'model' => 'datatable/customers_datatable_model', 'rowIdCol' => 'id'*/));

        $this->datatable->appendToSelectStr = array();
        $this->datatable->fromTableStr = $this->customers_model->t->balance_history;

        $this->datatable->whereClauseArray = array(
            'customer_id' => $this->input->post('customer_id')
        );

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson(array(
            'updated_at' => 'date',
            'created_at' => 'date',
        ));

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    /**
     * Ajax helper Get customers list
     * Server side datatable
     */
    public function get_login_log()
    {
        if (!$this->input->is_ajax_request()) die();

        $this->load->library('Datatable', array(/*'model' => 'datatable/customers_datatable_model', 'rowIdCol' => 'id'*/));

        $this->datatable->appendToSelectStr = array();
        $this->datatable->fromTableStr = $this->customers_model->t->tokens_log;

        $this->datatable->whereClauseArray = array(
            'account_id' => $this->input->post('customer_id')
        );

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson(array(
            'created_at' => 'date'
        ));

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

}
