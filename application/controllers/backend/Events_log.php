<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events_log extends AdminController
{

    function __construct()
    {
        parent::__construct();
        $this->data['page_header'] = 'Events Log';

        $this->load->model('events_log_model');
    }


    /**
     * Events list
     */
    public function index()
    {
        $this->data['page_desc'] = 'Events Log List';

        $this->twig->display('events_log/index.twig', $this->data);
    }


    /**
     * Event info page
     *
     * @param $id
     */
    public function info($id)
    {
        $this->data['event'] = $this->events_log_model->get($id);

        $this->twig->display('events_log/info.twig', $this->data);
    }


    /**
     * Get events list via post ajax
     * Server side datatable
     *
     * @throws Exception
     */
    public function get_events()
    {
        if (!$this->input->is_ajax_request()) die();

        $this->load->library('Datatable', array(/*'model' => 'datatable/players_datatable_model', 'rowIdCol' => 'id'*/));

        $this->datatable->appendToSelectStr = array();
        $this->datatable->fromTableStr = $this->events_log_model->t->events_log . ' events_log';
/*        $this->datatable->joinArray = array(
            $this->accounts_model->t->players . ' players|left' => 'players.account_id = accounts.id'
        );*/

        //format array is optional, but shown here for the sake of example
        $json = $this->datatable->datatableJson(array(
            'created_at' => 'date'
        ));

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    
}
