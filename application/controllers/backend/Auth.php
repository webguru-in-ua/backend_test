<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends AuthController
{

    function __construct()
    {
        parent::__construct();
        $this->twig->set_layout('backend');
    }


    /**
     * Main Page
     */
    public function index()
    {
        $this->login();
    }


    /**
     * Login Page
     */
    public function login()
    {

        if ($this->ion_auth->logged_in()) redirect('backend', 'refresh');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('identity', 'Login', 'required|trim|max_length[128]');
        $this->form_validation->set_rules('identity', 'Password', "required");

        if ($this->form_validation->run() == FALSE) {
            $this->data['validation_errors'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('validation_errors');
        } else {
            $remember = (bool)$this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
//                if ($this->auth_lib->in_group('admin'))
//                    redirect('admin', 'refresh');
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            } else {
                $this->data['validation_errors'] = $this->ion_auth->errors();

                //var_dump($this->auth_lib->errors());
                //redirect('auth/login', 'refresh');
            }
        }

        $this->data['messages'] = $this->session->flashdata('messages');
        $this->twig->display('auth/login.twig', $this->data);
    }


    /**
     * User Logout
     */
    function logout()
    {
        //log the user out
        $this->ion_auth->logout();

        //redirect them to the login page
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('backend/auth/login', 'refresh');
    }


    /**
     * Password recovery, create recovery link and send email
     */
    public function recovery()
    {

        $this->form_validation->set_rules('email', 'Email', 'required');
        if ($this->form_validation->run() == false) {
            //if there were no errors
            $this->data['validation_errors'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('validation_errors');

        } else {
            $data = $this->ion_auth->forgotten_password($this->input->post('email'));
            if ($data) {

                // send email
                $this->load->library('swift_mailer_lib');

                $this->swift_mailer_lib->from($this->config->item('reset_password')['outbox']);
                $this->swift_mailer_lib->to($this->input->post('email'));
                $this->swift_mailer_lib->subject($this->config->item('reset_password')['subject']);

                $body = $this->twig->render('emails/forgot_password.twig', $data);

                $this->swift_mailer_lib->message($body);

                if ($this->swift_mailer_lib->send()) {

                } else {
                    //log
                }

                $this->session->set_flashdata('messages', $this->ion_auth->messages());
                redirect("backend/auth", 'refresh');
            } else {

                $this->session->set_flashdata('validation_errors', $this->ion_auth->errors());
                redirect("backend/auth/recovery", 'refresh');
            }

        }

        $this->twig->display('auth/recovery.twig', $this->data);
    }


    /**
     * Reset password form
     * @param null $code
     */
    public function reset_password($code = NULL)
    {
        if (!$code) {
            redirect("backend/auth/recovery", 'refresh');
        }

        $reset = $this->ion_auth->forgotten_password_complete($code);

        if ($reset) {  //if the reset worked then send them to the login page
            $this->session->set_flashdata('message', $this->ion_auth->messages());

            $this->data['new_password'] = $reset['new_password'];

            $this->twig->display('auth/password.twig', $this->data);

            //redirect("backend/auth", 'refresh');
        } else { //if the reset didnt work then send them back to the forgot password page
            $this->session->set_flashdata('validation_errors', $this->ion_auth->errors());
            redirect("backend/auth/recovery", 'refresh');
        }

        /*
                if (($user = $this->auth_lib->resetPassword($code)) == false) {

                    //if the code is invalid then send them back to the forgot password page
                    $this->session->set_flashdata('validation_errors', $this->auth_lib->errors());
                    redirect("auth/recovery", 'refresh');
                }


                //if the code is valid then display the password reset form
                $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
                $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');


                if ($this->form_validation->run() == false) {
                    //set the flash data error message if there is one
                    $this->data['validation_errors'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('validation_errors');

                    $this->data['code'] = $code;
                    //render
                    $this->twig->display('auth/reset_password.twig', $this->data);

                } else {

                    if ($this->auth_lib->changePassword($this->input->post('new'), $user->id)) {
                        $this->session->set_flashdata('messages', $this->auth_lib->messages());
                        redirect('auth/login', 'refresh');
                    } else {
                        $this->session->set_flashdata('validation_errors', $this->auth_lib->errors());
                        redirect('auth/reset_password/' . $code, 'refresh');
                    }
                }*/

    }

}