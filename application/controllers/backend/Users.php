<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends AdminController
{

    function __construct()
    {
        parent::__construct();

        $this->data['page_header'] = 'Users';
        $this->data['page_desc'] = 'by group';
    }


    /**
     * Index page
     */
    public function index()
    {
        $this->group(1);
        return TRUE;

    }


    /**
     * Users list by group
     * @param $group_id
     * @return bool
     */
    public function group($group_id)
    {

        $this->data['group'] = $this->ion_auth->group($group_id)->row();

        //need log, notify error, redirect main page
        if (!$this->data['group'])
            return false;

        $this->data['users'] = $this->ion_auth->users(array($group_id), false)->result();
        $this->data['deleted_users'] = $this->ion_auth->users(array($group_id), true)->result();

        $this->twig->display('users/index.twig', $this->data);
    }


    /**
     * Create user
     */
    public function create()
    {
        $this->data['page_desc'] = "Create User";

        $tables = $this->config->item('tables', 'ion_auth');

        //validate form input
        $this->form_validation->set_rules('first_name', 'First name', 'required');
        $this->form_validation->set_rules('last_name', 'Last name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
//        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required');
//        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Password confirm', 'required');

        if ($this->form_validation->run() == true) {
            $username = strtolower($this->input->post('first_name')) . '_' . strtolower($this->input->post('last_name'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
//                'company'    => $this->input->post('company'),
//                'phone'      => $this->input->post('phone'),
            );
            $groupData = ($this->input->post('groups')) ? $this->input->post('groups') : array();


            $registration_data = $this->ion_auth->register($username, $password, $email, $additional_data, $groupData);
            if ($registration_data !== FALSE) {
                //send email this if needed


                //check to see if we are creating the user
                //redirect them back to the admin page
                $this->session->set_flashdata('messages', $this->ion_auth->messages());

                if (!empty($groupData))
                    redirect(backend_url("users/group/" . $groupData[0]), 'refresh');
                else
                    redirect(backend_url("users/group/1"), 'refresh');

            } else {
                $this->data['validation_errors'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            }


        } else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['validation_errors'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        }

        $this->twig->display('users/create.twig', $this->data);

    }


    /**
     * Edit user by id
     * @param null $user_id
     */
    public function edit($user_id = NULL)
    {
        $this->load->library('form_validation');
        $this->data['page_desc'] = "Edit user profile";

        // Edit profile if id not set
        if ($user_id == NULL && isset($this->data['_user']->id)) {
            $user_id = $this->data['_user']->id;
        }

        $user = $this->ion_auth->user($user_id)->row();
        $currentGroups = $this->ion_auth->get_users_groups($user_id)->result();

        //validate form input
        $this->form_validation->set_rules('first_name', 'First name', 'required');
        $this->form_validation->set_rules('last_name', 'Last name', 'required');
//        $this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
//        $this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');


        //update the password if it was posted
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'Password confirm', 'required');
        }

        if ($this->form_validation->run() === TRUE) {


            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
//                    'company' => $this->input->post('company'),
//                    'phone' => $this->input->post('phone'),
            );

            //update the password if it was posted
            if ($this->input->post('password')) {
                $data['password'] = $this->input->post('password');
            }


            // Only allow updating groups if user is admin
            //if ($this->ion_auth->is_admin())

            //Update the groups user belongs to
            $groupData = $this->input->post('groups');

            if (isset($groupData) && !empty($groupData)) {

                $this->ion_auth->remove_from_group('', $user_id);

                foreach ($groupData as $grp) {
                    $this->ion_auth->add_to_group($grp, $user_id);
                }

            }

            //check to see if we are updating the user
            if ($this->ion_auth->update($user->id, $data)) {
                //redirect them back to the admin page if admin, or to the base url if non admin
                $this->session->set_flashdata('messages', $this->ion_auth->messages());

                if ($this->ion_auth->is_admin()) {
                    redirect('backend/users/edit/' . $user->id, 'refresh');
                } else {
                    redirect('backend', 'refresh');
                }

            } else {
                //redirect them back to the admin page if admin, or to the base url if non admin
                $this->session->set_flashdata('messages', $this->ion_auth->errors());

                if ($this->ion_auth->is_admin()) {
                    redirect('backend/users/edit/' . $user->id, 'refresh');
                } else {
                    redirect('backend', 'refresh');
                }

            }

        }


        //display the edit user form
        //$this->data['csrf'] = $this->_get_csrf_nonce();

        //set the flash data error message if there is one
        $this->data['validation_errors'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));


        //pass the user to the view
        $this->data['user'] = $user;

        $this->data['currentGroups'] = $currentGroups;

//        $this->_render_page('auth/edit_user', $this->data);
        $this->twig->display('users/edit.twig', $this->data);
    }


    /**
     * Delete user by id
     * @param $user_id
     */
    public function delete($user_id)
    {

        $currentGroups = $this->ion_auth->get_users_groups($user_id)->result();

        if ($user_id === $this->data['_user']->id) {
            redirect(backend_url('users/group/' . $currentGroups[0]->id));
            return FALSE;
        }

        $user = $this->ion_auth->user($user_id)->row();
        if ($user->deleted == 1)
            $this->ion_auth->delete_user($user_id, false);
        else
            $this->ion_auth->delete_user($user_id);

        if (!empty($currentGroups)) {
            redirect(backend_url('users/group/' . $currentGroups[0]->id));
        }

        redirect(backend_url());
    }

}