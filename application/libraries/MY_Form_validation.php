<?php

class MY_Form_validation extends CI_Form_validation
{
    function __construct()
    {
        parent::__construct();
    }

    function not_in_list($value, $list)
    {
        return !in_array($value, explode(',', $list), TRUE);
    }

    function valid_date($date)
    {
        if (date('m/d/Y', strtotime($date)) == $date) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function valid_time($time)
    {
        if (date('H:i', strtotime($time)) == $time) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    function valid_fb_link($str)
    {
        $pattern = "|^[a-zA-Z0-9?=&_.\-//]+$|i";
        if (!preg_match($pattern, $str)) {
            return FALSE;
        }

        return TRUE;
    }


    function valid_json($str)
    {
        if (json_decode($str) === NULL || json_decode($str) === FALSE) {
            return FALSE;
        }
        return TRUE;
    }


    /**
     * Check color (#fff, #ffffff, red)
     * @param $str
     * @return bool
     */
    function valid_color($str)
    {
        $str = strtolower($str);

        if (strpos($str, '#') === 0) {
            if (preg_match('/^#(?:[0-9a-fA-F]{6}|[0-9a-fA-F]{3})$/i', $str))
                return TRUE;
        } else {
            $named = array('aliceblue', 'antiquewhite', 'aqua', 'aquamarine', 'azure', 'beige', 'bisque', 'black', 'blanchedalmond', 'blue', 'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate', 'coral', 'cornflowerblue', 'cornsilk', 'crimson', 'cyan', 'darkblue', 'darkcyan', 'darkgoldenrod', 'darkgray', 'darkgreen', 'darkkhaki', 'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen', 'darkslateblue', 'darkslategray', 'darkturquoise', 'darkviolet', 'deeppink', 'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'floralwhite', 'forestgreen', 'fuchsia', 'gainsboro', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'greenyellow', 'honeydew', 'hotpink', 'indianred', 'indigo', 'ivory', 'khaki', 'lavender', 'lavenderblush', 'lawngreen', 'lemonchiffon', 'lightblue', 'lightcoral', 'lightcyan', 'lightgoldenrodyellow', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen', 'lightskyblue', 'lightslategray', 'lightsteelblue', 'lightyellow', 'lime', 'limegreen', 'linen', 'magenta', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid', 'mediumpurple', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise', 'mediumvioletred', 'midnightblue', 'mintcream', 'mistyrose', 'moccasin', 'navajowhite', 'navy', 'oldlace', 'olive', 'olivedrab', 'orange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink', 'plum', 'powderblue', 'purple', 'red', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen', 'seashell', 'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'snow', 'springgreen', 'steelblue', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'wheat', 'white', 'whitesmoke', 'yellow', 'yellowgreen');

            if (in_array($str, $named)) {
                /* A color name was entered instead of a Hex Value, so just exit function */
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @param string $str
     * @return bool
     */
    public function valid_url($str)
    {
        if (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches)) {
            if (empty($matches[2])) {
                return FALSE;
            } elseif (!in_array($matches[1], array('http', 'https'), TRUE)) {
                return FALSE;
            }

            //$str = $matches[2];

            return TRUE;
        } else {
            return FALSE;
        }

        // There's a bug affecting PHP 5.2.13, 5.3.2 that considers the
        // underscore to be a valid hostname character instead of a dash.
        // Reference: https://bugs.php.net/bug.php?id=51192
        if (version_compare(PHP_VERSION, '5.2.13', '==') OR version_compare(PHP_VERSION, '5.3.2', '==')) {
            sscanf($str, 'http://%[^/]', $host);
            $str = substr_replace($str, strtr($host, array('_' => '-', '-' => '_')), 7, strlen($host));
        }


        return (filter_var($str, FILTER_VALIDATE_URL) !== FALSE);
    }


    /**
     * @param $str
     * @param $field
     * @return bool
     */
    public function array_unique($str, $field)
    {
        return (count(array_unique($_POST[$field])) == count($_POST[$field]));
    }

}
