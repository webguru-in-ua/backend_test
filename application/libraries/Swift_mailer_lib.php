<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Swift_mailer_lib
{
    protected $CI;

    public $message;
    public $mailer;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->config('swift_mailer');

        // Init swift mailer
        $transport = Swift_SmtpTransport::newInstance(
            $this->CI->config->item('smtp_host'),
            $this->CI->config->item('smtp_port'),
            $this->CI->config->item('smtp_encryption')
        );
        $transport->setUsername($this->CI->config->item('smtp_login'));
        $transport->setPassword($this->CI->config->item('smtp_password'));


        $this->mailer = Swift_Mailer::newInstance($transport);
        $this->message = Swift_Message::newInstance();
    }


    /**
     * @param $subject
     */
    public function subject($subject)
    {
        $this->message->setSubject($subject);
    }

    /**
     * @param $from
     */
    public function from($from)
    {
        $this->message->setFrom($from);
    }

    /**
     * @param $from
     */
    public function to($from)
    {
        $this->message->setTo($from);
    }

    /**
     * @param $message
     * @param string $type
     */
    public function message($message, $type = 'text/html')
    {
        $this->message->setBody($message, $type);
    }


    /**
     * @param null $message
     * @return int
     */
    public function send($message = NULL)
    {
        if (!is_null($message))
            $this->message = $message;
        return $this->mailer->send($this->message);
    }
}