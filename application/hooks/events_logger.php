<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_logger
{

    public function log_all()
    {
        $CI = &get_instance();

        if ($CI->uri->segment(1) == 'backend' && !empty($CI->input->post(null)) && !is_null($CI->input->post('event_track'))) {

            $event_track_data = array(
                'name' => $CI->input->post('event_track_name'),
                'page' => $CI->input->post('event_track_page'),
                'target' => $CI->input->post('event_track_target'),
                'action' => $CI->input->post('event_track_action')
            );

            unset($_POST['event_track']);
            unset($_POST['event_track_name']);
            unset($_POST['event_track_page']);
            unset($_POST['event_track_action']);
            unset($_POST['event_track_target']);

            if (empty($event_track_data['target'])) {
                //'page' => uri_string(),
                // get current clear url
                $event_track_data['target'] = $CI->uri->segment_array();
                array_shift($event_track_data['target']);
                $event_track_data['target'] = '/' . implode('/', $event_track_data['target']);
            }

            // prepare $_POST data to storage
            $post = var_export($CI->input->post(null), true);

            $event_data = array(
                'session_id' => session_id(),
                'user_id' => $CI->data['_user']->id,
                'user_email' => $CI->data['_user']->email,
                'url' => current_url(),
                'page' => $event_track_data['page'],
                'target' => $event_track_data['target'],
                'name' => $event_track_data['name'],
                'action' => $event_track_data['action'],
                'post' => $post,
                'ip' => $CI->input->ip_address()
            );

            $CI->load->model('events_log_model');
            $CI->events_log_model->add($event_data);
        }
    }
}