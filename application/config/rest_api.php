<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['rest_api_skip_access_token'] = array(
);

$config['rest_api_show_execution_time'] = true;
$config['allow_token_header_response'] = true;