<?php  if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/*
 * CodeIgniter Monolog integration
 *
 * (c) Steve Thomas <steve@thomasmultimedia.com.au>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* GENERAL OPTIONS */
$config['monolog_handlers'] = array('file'); // set 'file' to write local logs, 'elastic' - send logs to elastic search server
$config['monolog_elasticsearch_client'] = ''; // set link to elasticSearch
$config['monolog_channel'] = ENVIRONMENT; // channel name which appears on each line of log
$config['monolog_type'] = 'APPLICATION'; // type name which appears on each line of log
$config['monolog_skip_functions'] = array( 'log_message', 'write_log', 'log_exception', 'call_user_func', 'call_user_func_array', '_remap');

/* FILE HANDLER OPTIONS
 * Log to default CI log directory (must be writable ie. chmod 757).
 * Filename will be encoded to current system date, ie. YYYY-MM-DD-ci.log
*/
$config['monolog_file_logfile'] = APPPATH . 'logs/monolog.log';

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|       'ERROR' => 1,
        'WARNING' => 1,
        'PARSE' => 1,
        'NOTICE' => 1,
        'DEBUG' => 2,
        'INFO' => 3,
        'ALL' => 4
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['monolog_client_log_threshold'] = 2;

/*
| Errors LEVEL set in config.php
| Important! Theshold set in config.php
| line 214
| $config['log_threshold'] = 4;
*/

// Skipped function for errors backtrace

//$config['introspection_processor'] = FALSE; // add some meta data such as controller and line number to log messages
$config['monolog_kibana_url'] = '';