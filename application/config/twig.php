<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Twig configuration
 */
$config['twig_template_dir'] = APPPATH.'views';
$config['twig_debug'] = TRUE;
$config['twig_cache_dir'] = APPPATH.'cache/twig';
$config['twig_cache'] = FALSE;