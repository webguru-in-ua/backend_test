<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by kramarenkoa@digicode.net (admin.kramarenko@gmail.com)
 */

$config['database_tables_prefix'] = '';
$config['database_tables_list'] = array(
    'customers'       => 'customers',
    'transactions'    => 'transactions',
    'transactions_sum'=> 'transactions_sum',
    'users'           => 'users', // system users (admins)
    'events_log'      => 'events_log',
    'error_codes'     => 'error_codes',
);